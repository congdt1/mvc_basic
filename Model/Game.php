<?php
include_once("E_Game.php");

class Model_Game
{
	public function __construct()
	{}
	
	public function getAllGame()
	{
		//Gia su rang ta load data tu CSDL
		return array(
			"1" => new Entity_Student(1, "LOL", 22, "1.jpg", "Riot"),
			"2" => new Entity_Student(2, "CS-Go", 23, "2.jpg", "ABC"),
			"3" => new Entity_Student(3, "BOOM", 21, "3.jpg", "ABC"),
		);
	}
	
	public function getGameDetail($stid)
	{
		//Gia su rang ta load data tu CSDL
		$allGame = $this->getAllGame();
		return $allGame[$stid];
	}
}

?>