<?php

class Entity_Student
{
	public $id;
	public $name;
	public $price;
	public $image;
	public $detail;
	
	public function __construct($_id, $_name, $_price, $_image, $_detail)
	{
		$this->id = $_id;
		$this->name = $_name;
		$this->price = $_price;
		$this->image = $_image;
		$this->detail = $_detail;
	}
}

?>