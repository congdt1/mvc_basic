<?php

include_once("../Model/Game.php");

//1. Declaration

class  Ctrl_Game
{
	public function invoke(){
		if(isset($_GET['stid']))
		{
			$modelGame =  new Model_Game();
			$game = $modelGame->getGameDetail($_GET['stid']);
			
			include_once("../View/GameDetail.html");
		}
		else
		{
			$modelGame =  new Model_Game();
			$gamelist = $modelGame->getAllGame();
			
			include_once("../View/GameList.html");
		}
	}
};


//////////////////////////////////////
//2. Process

$GameController = new Ctrl_Game();
$GameController->invoke();